package main

import (
	"log"

	"gitlab.com/galileoluna/stublockchain/database"
	"gitlab.com/galileoluna/stublockchain/handlers"
)

func main() {
	if database.ChequeoConnection() == 0 {
		log.Fatal("Sin conexion a la BD")
		return
	}
	handlers.Manejadores()
}
