module gitlab.com/galileoluna/stublockchain

go 1.15

require (
	github.com/gorilla/mux v1.8.0
	github.com/rs/cors v1.7.0
	gitlab.com/galileoluna/apiUNGS v0.0.0-20200925001225-e5adde5ac942
	go.mongodb.org/mongo-driver v1.4.4
)
