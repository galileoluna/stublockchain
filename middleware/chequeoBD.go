package middleware

import (
	"net/http"

	"gitlab.com/galileoluna/stublockchain/database"
)

// Chequeo la base de datos
func ChequeoBD(next http.HandlerFunc) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {
		if database.ChequeoConnection() == 0 {
			http.Error(w, "Conexion perdida", 500)
			return
		}
		next.ServeHTTP(w, r)
	}
}
