package routers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/galileoluna/stublockchain/models"
)

// Creamos en la base de datos la inscripcion

func Inscripcion(w http.ResponseWriter, r *http.Request) {

	var t models.Inscripcion
	err := json.NewDecoder(r.Body).Decode(&t)

	if err != nil {
		http.Error(w, "Error en los datos recibidos"+err.Error(), 400)
		return
	}

	if len(t.Alumno) == 0 {
		http.Error(w, "El alumno es requerido", 400)
		return
	}

}
