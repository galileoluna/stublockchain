package handlers

import (
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"gitlab.com/galileoluna/stublockchain/middleware"
	"gitlab.com/galileoluna/stublockchain/routers"
)

// Configuro los handlers
func Manejadores() {

	router := mux.NewRouter()

	router.HandleFunc("/inscripcion", middleware.ChequeoBD(routers.Inscripcion)).Methods("POST")

	PORT := os.Getenv("PORT")
	if PORT == "" {
		PORT = "8080"
	}
	//DAMOS Permiso a todo el mundo
	handler := cors.AllowAll().Handler(router)
	log.Fatal(http.ListenAndServe(":"+PORT, handler))

}
